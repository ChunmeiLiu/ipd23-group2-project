﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FurnitureStoreSales.Tests
{
    [TestClass]
    public class ModelTest
    {

        [TestMethod]
        public void Customer_NewTest_ReturnTrue()
        {
            //arrange
            Customer cTest = new Customer();
            cTest.id = 10;
            cTest.FirstName = "User1";
            cTest.LastName = "UnitTest";
            cTest.Address = "50 Boul College";
            cTest.PostalCode = "H0H 1O1";
            cTest.Telephone = "4302903456";
            cTest.Email = "U.Unit@test.net";
            cTest.MemberCard = "TM0001";
            cTest.Balance = 1.23M;

            //act

            //assert
            Assert.AreEqual("User1", cTest.FirstName);
            Assert.AreEqual("UnitTest", cTest.LastName);
            Assert.AreEqual("50 Boul College", cTest.Address);
            Assert.AreEqual("H0H 1O1", cTest.PostalCode);
            Assert.AreEqual("4302903456", cTest.Telephone);
            Assert.AreEqual(1.23M, cTest.Balance);
        }

        [TestMethod]
        public void Purchase_NewTest_ReturnTrue()
        {
            //arrange
            Purchase pTest = new Purchase();
            pTest.id = 100;
            pTest.ProductID = 110;
            pTest.ProductNumber = 5;
            pTest.Total = 895.98M;
            pTest.OrderDate = new System.DateTime(01 / 15 / 2021);
            pTest.PurchaseDate = null;
            //act

            //assert
            Assert.AreEqual(110, pTest.ProductID);
            Assert.AreEqual(5, pTest.ProductNumber);
            Assert.AreEqual(895.98M, pTest.Total);
            Assert.AreEqual(new System.DateTime(01 / 15 / 2021), pTest.OrderDate);
            Assert.AreEqual(null, pTest.PurchaseDate);
        }

        [TestMethod]
        public void Order_NewTest_ReturnTrue()
        {
            //arrange
            OrderItem oTest = new OrderItem();
            oTest.id = 100;
            oTest.OrderID = 10;
            oTest.ProductID = 101;
            oTest.ItemCount = 3;
            oTest.Price = 129.9M;
            oTest.SubTotal = oTest.ItemCount * oTest.Price;

            //act

            //assert
            Assert.AreEqual(10, oTest.OrderID);
            Assert.AreEqual(101, oTest.ProductID);
            Assert.AreEqual(129.9M, oTest.Price);
            Assert.AreEqual(3 * 129.9M, oTest.SubTotal);
        }

    }
}
