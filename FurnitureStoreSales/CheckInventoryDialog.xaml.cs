﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for CheckInventoryDialog.xaml
    /// </summary>
    public partial class CheckInventoryDialog : Window
    {
        public CheckInventoryDialog()
        {
            InitializeComponent();
            Globals.ctx = new FurnitureStoreSalesEntities();
            FetchInventoriesRecords();

        }
        public void FetchInventoriesRecords()
        {
            try
            {
                lvIvtProductList.ItemsSource = Globals.ctx.Inventories.ToList();
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
           


        }

        private void btIvtGo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int limitStock = int.Parse(tbLimitStock.Text);


                lvProductList_check.ItemsSource = Globals.ctx.Inventories.Where(p => p.Quantity < limitStock).ToList();
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btIvtExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btIvtBackToMainWindow_Click(object sender, RoutedEventArgs e)
        {
            Welcome w  = new Welcome() { Owner = this };
            w.ShowDialog();
            this.Close();


        }
    }
}
