﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        public Login()
        {
            InitializeComponent();
        }

        private void LoginApp()
        {
            throw new NotImplementedException();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
                if (validateLogin())
                {
                    MainWindow main = new MainWindow();
                    main.Show();
                    this.Close();

                }
                else
                    MessageBox.Show("Incorrect user name or password");
        }

        private bool validateLogin()
        {
           /*
             * 
             try
            {
                Globals.ctx = new FurnitureStoreSalesConnection();
                String query = "SELECT COUNT(1) FROM USER WHERE USERNAME=@USERNAME AND PASSWORD=@PASSWORD";
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@USERNAME",tbUsername.Text);
                sqlCmd.Parameters.AddWithValue("@PASSWORD",pbPassword.Password);
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                if(count == 1)
                   return true;
                return false;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
             
             */
            if (tbUsername.Text == "admin")
                if (pbPassword.Password == "1234")
                    return true;
            return false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            NewUser wizWin = new NewUser();  
            this.Close();
            wizWin.ShowDialog();
            
        }
    }
}

