﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //byte[] currProductImage;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                FetchProductsRecords();
                FetchCustomersRecords();
                FetchPurchasesResultRecords();
                FetchOrdersResultRecords();
                FetchSuppliersRecords();
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal error
            }
        }


        public void FetchCustomersRecords()
        {
            try
            {
                lvCustomerList.ItemsSource = Globals.ctx.Customers.ToList();
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void FetchPurchasesRecords()
        {
            try
            {

                lvPurchasesList.ItemsSource = Globals.ctx.Purchases.ToList(); //sc; // records;
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void FetchPurchasesResultRecords()
        {
            try
            {

                lvPurchasesList.ItemsSource = Globals.ctx.GetPurchase();  //GetPurchase_Result.ToList(); //sc; // records;
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void FetchOrdersResultRecords()
        {
            try
            {

                lvOrderList.ItemsSource = Globals.ctx.GetOrders();  //GetPurchase_Result.ToList(); //sc; // records;
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        //===========Product section============================================================
        public void FetchProductsRecords()
        {
            try
            {

                lvProductList.ItemsSource = Globals.ctx.Products.ToList();

            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btAddProduct_Click(object sender, RoutedEventArgs e)
        {
            ManageProductDialog mDlg = new ManageProductDialog() { Owner = this };
            mDlg.btnDialogAdd.IsEnabled = true;
            mDlg.btnDialogUpdate.IsEnabled = false;
            mDlg.btnDialogDelete.IsEnabled = false;
            mDlg.ShowDialog();


        }
        private void btManageProduct_Click(object sender, RoutedEventArgs e)
        {
            ManageProductDialog mDlg = new ManageProductDialog() { Owner = this };

            if (lvProductList.SelectedItems != null)
            {
                Product productSelected = (Product)lvProductList.SelectedItem;
                if (productSelected == null) { return; }

                byte[] currProductImage = productSelected.ProductPicture;
                mDlg.imageViewer.Source = Utils.GetBitmapImageV2(productSelected.ProductPicture);
                mDlg.tbProductId.Text = productSelected.ProductId.ToString();
                mDlg.tbProductId.IsReadOnly = true;
                mDlg.tbProductName.Text = productSelected.ProductName;
                mDlg.tbCategory.Text = productSelected.Category;
                mDlg.tbPrice.Text = productSelected.Price.ToString();
                mDlg.tbDescription.Text = productSelected.ProductDescription;
                mDlg.btnDialogAdd.IsEnabled = false;
                mDlg.btnDialogUpdate.IsEnabled = true;
                mDlg.btnDialogDelete.IsEnabled = true;
                mDlg.ShowDialog();
            }
         
        }

        public void lvProductList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Product productSelected = (Product)lvProductList.SelectedItem;
            if (productSelected == null) { return; }
            ManageProductDialog mDlg = new ManageProductDialog() { Owner = this };

            byte[] currProductImage = productSelected.ProductPicture;
            mDlg.imageViewer.Source = Utils.GetBitmapImageV2(productSelected.ProductPicture);
            mDlg.tbProductId.Text = productSelected.ProductId.ToString();
            mDlg.tbProductId.IsReadOnly = true;
            mDlg.tbProductName.Text = productSelected.ProductName;
            mDlg.tbCategory.Text = productSelected.Category;
            mDlg.tbPrice.Text = productSelected.Price.ToString();
            mDlg.tbDescription.Text = productSelected.ProductDescription;
            mDlg.tbPdtSupplierId.Text = productSelected.SupplierId.ToString();
            mDlg.btnDialogAdd.IsEnabled = false;
            mDlg.btnDialogUpdate.IsEnabled = true;
            mDlg.btnDialogDelete.IsEnabled = true;
            mDlg.ShowDialog();

        }

        private void btProductDetail_Click(object sender, RoutedEventArgs e)
        {
            ManageProductDialog mDlg = new ManageProductDialog() { Owner = this };

            if (lvProductList.SelectedItems != null)
            {
                Product productSelected = (Product)lvProductList.SelectedItem;
                if (productSelected == null) { return; }

                byte[] currProductImage = productSelected.ProductPicture;
                mDlg.imageViewer.Source = Utils.GetBitmapImageV2(productSelected.ProductPicture);
                mDlg.tbProductId.Text = productSelected.ProductId.ToString();
                mDlg.tbProductId.IsReadOnly = true;
                mDlg.tbProductName.Text = productSelected.ProductName;
                mDlg.tbCategory.Text = productSelected.Category;
                mDlg.tbPrice.Text = productSelected.Price.ToString();
                mDlg.tbDescription.Text = productSelected.ProductDescription;
                mDlg.btnDialogAdd.Visibility = Visibility.Hidden;
                mDlg.btnDialogAdd.Visibility = Visibility.Collapsed;
                mDlg.btnDialogUpdate.Visibility = Visibility.Hidden;
                mDlg.btnDialogUpdate.Visibility = Visibility.Collapsed;
                mDlg.btnDialogDelete.Visibility = Visibility.Hidden;
                mDlg.btnDialogDelete.Visibility = Visibility.Collapsed;
                mDlg.ShowDialog();
            }
        }

        private void btRefresh_Click(object sender, RoutedEventArgs e)
        {
            FetchProductsRecords();

        }

        //======================================================================================================

        //=============Supplier section=========================================================================
        public void FetchSuppliersRecords()
        {
            try
            {

                lvSupplierList.ItemsSource = Globals.ctx.Suppliers.ToList();
            }
            catch (SystemException ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btManageSupplier_Click(object sender, RoutedEventArgs e)
        {
            Supplier supplierSelected = (Supplier)lvSupplierList.SelectedItem;

            ManageSupplierDialog mDlg = new ManageSupplierDialog() { Owner = this };
            if (supplierSelected == null)
            {
                mDlg.btnmpAdd.IsEnabled = true;
                mDlg.btnmpUpdate.IsEnabled = false;
                mDlg.btnmpDelete.IsEnabled = false;
                mDlg.ShowDialog();
            }
            mDlg.tbmpSupplierId.Text = supplierSelected.SupplierId.ToString();
            mDlg.tbmpSupplierId.IsReadOnly = true;
            mDlg.tbmpSupplierName.Text = supplierSelected.CompanyName;
            mDlg.tbmpSupplierAddress.Text = supplierSelected.Address;
            mDlg.tbmpSupplierPostalCode.Text = supplierSelected.PostalCode;
            mDlg.tbSupplierTelephone.Text = supplierSelected.Telephone;
            mDlg.tbmpSupplierEmail.Text = supplierSelected.Email;
            mDlg.tbmpSupplierMemo.Text = supplierSelected.SupplierMemo;
            mDlg.btnmpAdd.IsEnabled = false;
            mDlg.btnmpUpdate.IsEnabled = true;
            mDlg.btnmpDelete.IsEnabled = true;
            mDlg.ShowDialog();
        }

        public void lvSupplierList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Supplier supplierSelected = (Supplier)lvProductList.SelectedItem;
            if (supplierSelected == null) { return; }
            ManageSupplierDialog mDlg = new ManageSupplierDialog() { Owner = this };

            mDlg.tbmpSupplierId.Text = supplierSelected.SupplierId.ToString();
            mDlg.tbmpSupplierId.IsReadOnly = true;
            mDlg.tbmpSupplierName.Text = supplierSelected.CompanyName;
            mDlg.tbmpSupplierAddress.Text = supplierSelected.Address;
            mDlg.tbmpSupplierPostalCode.Text = supplierSelected.PostalCode;
            mDlg.tbSupplierTelephone.Text = supplierSelected.Telephone;
            mDlg.tbmpSupplierEmail.Text = supplierSelected.Email;
            mDlg.tbmpSupplierMemo.Text = supplierSelected.SupplierMemo;
            mDlg.btnmpAdd.IsEnabled = false;
            mDlg.btnmpUpdate.IsEnabled = true;
            mDlg.btnmpDelete.IsEnabled = true;
            mDlg.ShowDialog();
        }


        private void btAddSupplier_Click(object sender, RoutedEventArgs e)
        {
            ManageSupplierDialog mDlg = new ManageSupplierDialog() { Owner = this };
            mDlg.btnmpAdd.IsEnabled = true;
            mDlg.btnmpUpdate.IsEnabled = false;
            mDlg.btnmpDelete.IsEnabled = false;
            mDlg.ShowDialog();
        }

        private void btSupplierDetail_Click(object sender, RoutedEventArgs e)
        {
            Supplier supplierSelected = (Supplier)lvSupplierList.SelectedItem;

            ManageSupplierDialog mDlg = new ManageSupplierDialog() { Owner = this };
            if (supplierSelected == null)
            {
                mDlg.btnmpAdd.IsEnabled = true;
                mDlg.btnmpUpdate.IsEnabled = false;
                mDlg.btnmpDelete.IsEnabled = false;
                mDlg.ShowDialog();
            }
            mDlg.tbmpSupplierId.Text = supplierSelected.SupplierId.ToString();
            mDlg.tbmpSupplierId.IsReadOnly = true;
            mDlg.tbmpSupplierName.Text = supplierSelected.CompanyName;
            mDlg.tbmpSupplierAddress.Text = supplierSelected.Address;
            mDlg.tbmpSupplierPostalCode.Text = supplierSelected.PostalCode;
            mDlg.tbSupplierTelephone.Text = supplierSelected.Telephone;
            mDlg.tbmpSupplierEmail.Text = supplierSelected.Email;
            mDlg.tbmpSupplierMemo.Text = supplierSelected.SupplierMemo;

            mDlg.btnmpAdd.Visibility = Visibility.Hidden;
            mDlg.btnmpAdd.Visibility = Visibility.Collapsed;
            mDlg.btnmpUpdate.Visibility = Visibility.Hidden;
            mDlg.btnmpUpdate.Visibility = Visibility.Collapsed;
            mDlg.btnmpDelete.Visibility = Visibility.Hidden;
            mDlg.btnmpDelete.Visibility = Visibility.Collapsed;
            mDlg.ShowDialog();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            FetchSuppliersRecords();
        }

        //===================================================================================================
        //============CheckInventory Button==================================================================

        private void btCheckInventory_Click(object sender, RoutedEventArgs e)
        {

            CheckInventoryDialog mDlg = new CheckInventoryDialog() { Owner = this };
            mDlg.ShowDialog();

        }

        //===================================================================================================



        private void lvCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            btnManageCustomer.IsEnabled = true;
        }

        private void lvCustomerList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Customer customerSelected = (Customer)lvCustomerList.SelectedItem;
            if (customerSelected == null) { return; }
            ManageCustomerDialog mDlg = new ManageCustomerDialog() { Owner = this };
            mDlg.tbCustomerId.Text = customerSelected.id.ToString();
            mDlg.tbFN.Text = customerSelected.FirstName;
            mDlg.tbLN.Text = customerSelected.LastName;
            mDlg.tbAddress.Text = customerSelected.Address;
            mDlg.tbPostCode.Text = customerSelected.PostalCode;
            mDlg.tbPhone.Text = customerSelected.Telephone;
            mDlg.tbEmail.Text = customerSelected.Email;
            mDlg.tbMemberCard.Text = customerSelected.MemberCard;
            mDlg.tbBalance.Text = customerSelected.Balance.ToString();
            mDlg.btnDialogAdd.IsEnabled = false;
            mDlg.btnDialogUpdate.IsEnabled = true;
            mDlg.btnDialogDelete.IsEnabled = true;
            mDlg.ShowDialog();
            FetchCustomersRecords();
        }
        private void btnManageCustomer_Click(object sender, RoutedEventArgs e)
        {
            ManageCustomerDialog mDlg = new ManageCustomerDialog() { Owner = this };

            if (lvCustomerList.SelectedItems != null)
            {
                Customer customerSelected = (Customer)lvCustomerList.SelectedItem;
                if (customerSelected == null) { return; }
                mDlg.tbCustomerId.Text = customerSelected.id.ToString();
                mDlg.tbFN.Text = customerSelected.FirstName;
                mDlg.tbLN.Text = customerSelected.LastName;
                mDlg.tbAddress.Text = customerSelected.Address;
                mDlg.tbPostCode.Text = customerSelected.PostalCode;
                mDlg.tbPhone.Text = customerSelected.Telephone;
                mDlg.tbEmail.Text = customerSelected.Email;
                mDlg.tbMemberCard.Text = customerSelected.MemberCard;
                mDlg.tbBalance.Text = customerSelected.Balance.ToString();
                mDlg.btnDialogAdd.IsEnabled = false;
                mDlg.btnDialogUpdate.IsEnabled = true;
                mDlg.btnDialogDelete.IsEnabled = true;
                mDlg.ShowDialog();

                FetchCustomersRecords();
            }
            else
            {
                mDlg.btnDialogAdd.IsEnabled = true;
                mDlg.btnDialogUpdate.IsEnabled = false;
                mDlg.btnDialogDelete.IsEnabled = false;
                mDlg.ShowDialog();

            }

        }



         
         
        private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            ManageCustomerDialog mDlg = new ManageCustomerDialog() { Owner = this };
            mDlg.ClearInputs();
            //mDlg.btnDialogAdd.IsEnabled = true;
            //mDlg.btnDialogUpdate.IsEnabled = false;
            //mDlg.btnDialogDelete.IsEnabled = false;
            mDlg.btnDialogAdd.Visibility = Visibility.Visible;
            mDlg.btnDialogUpdate.Visibility = Visibility.Hidden;
            mDlg.btnDialogDelete.Visibility = Visibility.Hidden;
            mDlg.ShowDialog();
            FetchCustomersRecords();
        }

        //add new customer by wizard
        //private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        //{
        //    //var wizWin = new NavigationWindow();
        //    NewCustomer wizWin = new NewCustomer() { Owner = this };
        //    wizWin.ShowDialog();

        //    //if (!string.IsNullOrEmpty(wizWin.FirstNameTextBox.Text))
        //    //    MessageBox.Show("Hello " + wizWin.FirstNameTextBox.Text + "!");

        //}




        private void lvPurchasesList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetPurchase_Result puchaseSelected = (GetPurchase_Result)lvPurchasesList.SelectedItem;
            if (puchaseSelected == null) { return; }
            ManagePurchaseDialog mDlg = new ManagePurchaseDialog() { Owner = this };
            mDlg.tbPurchaseId.Text = puchaseSelected.id.ToString();
            mDlg.tbProductID.Text = puchaseSelected.ProductID.ToString();
            mDlg.tbProductNumbers.Text = puchaseSelected.ProductNumber.ToString();
            mDlg.lblTotal.Content = puchaseSelected.Total.ToString();
            mDlg.tbOrderDate.Text = puchaseSelected.OrderDate.ToString();
            mDlg.tbPurchaseDate.Text = puchaseSelected.PurchaseDate.ToString();

            mDlg.btnDialogAdd.IsEnabled = false;
            mDlg.btnDialogUpdate.IsEnabled = true;
            mDlg.btnDialogDelete.IsEnabled = true;
            mDlg.ShowDialog();

            FetchPurchasesResultRecords();
        }
        private void btnManagePurchase_Click(object sender, RoutedEventArgs e)
        {
            ManagePurchaseDialog mDlg = new ManagePurchaseDialog() { Owner = this };

            if (lvPurchasesList.SelectedItems != null)
            {
                GetPurchase_Result puchaseSelected = (GetPurchase_Result)lvPurchasesList.SelectedItem;
                if (puchaseSelected == null) { return; }
                mDlg.tbPurchaseId.Text = puchaseSelected.id.ToString();
                mDlg.tbProductID.Text = puchaseSelected.ProductID.ToString();
                mDlg.tbProductNumbers.Text = puchaseSelected.ProductNumber.ToString();
                mDlg.lblTotal.Content = puchaseSelected.Total.ToString();
                mDlg.tbOrderDate.Text = puchaseSelected.OrderDate.ToString();
                mDlg.tbPurchaseDate.Text = puchaseSelected.PurchaseDate.ToString();

                mDlg.btnDialogAdd.IsEnabled = false;
                mDlg.btnDialogUpdate.IsEnabled = true;
                mDlg.btnDialogDelete.IsEnabled = true;
                mDlg.ShowDialog();

                FetchPurchasesResultRecords();
            }
            else
            {
                mDlg.btnDialogAdd.IsEnabled = true;
                mDlg.btnDialogUpdate.IsEnabled = false;
                mDlg.btnDialogDelete.IsEnabled = false;
                mDlg.ShowDialog();

            }
        }
        private void btnAddPurchase_Click(object sender, RoutedEventArgs e)
        {
            ManagePurchaseDialog mDlg = new ManagePurchaseDialog() { Owner = this };
            mDlg.ClearInputs();
            //mDlg.btnDialogAdd.IsEnabled = true;
            //mDlg.btnDialogUpdate.IsEnabled = false;
            //mDlg.btnDialogDelete.IsEnabled = false;
            mDlg.btnDialogAdd.Visibility = Visibility.Visible;
            mDlg.btnDialogUpdate.Visibility = Visibility.Hidden;
            mDlg.btnDialogDelete.Visibility = Visibility.Hidden;

            mDlg.ShowDialog();
            FetchPurchasesResultRecords();
        }
        private void lvPurchasesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnManagePurchase.IsEnabled = true;
        }

        private void lvOrderList_MouseDoubleClick(object sender,MouseButtonEventArgs e)
        {
            GetOrders_Result orderSelected = (GetOrders_Result)lvOrderList.SelectedItem;
            ManageOrderDialog mWindow = new ManageOrderDialog(orderSelected.OrderID) { Owner = this };
            mWindow.Show();
        }

        private void btnManagerOrder_Click(object sender, RoutedEventArgs e)
        {
            GetOrders_Result orderSelected = (GetOrders_Result)lvOrderList.SelectedItem;
            ManageOrderDialog mWindow = new ManageOrderDialog(orderSelected.OrderID) { Owner = this };
            mWindow.Show();
            FetchOrdersResultRecords();
        }
        
        private void lvOrderList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnManagerOrder.IsEnabled = true;
        }

    }
}
