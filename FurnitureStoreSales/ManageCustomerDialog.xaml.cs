﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManageCustomerDialog.xaml
    /// </summary>
    public partial class ManageCustomerDialog : Window
    {
        
        public ManageCustomerDialog()//int CustomerId)
        {
            InitializeComponent();
            
        }
        
        private void btnDialogAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Customer c = new Customer();
                c.id = int.Parse(tbCustomerId.Text);
                c.FirstName = tbFN.Text;
                c.LastName = tbLN.Text;
                c.Address = tbAddress.Text;
                c.PostalCode = tbPostCode.Text;
                c.Telephone = tbPhone.Text;
                c.Email = tbEmail.Text;
                c.MemberCard = tbMemberCard.Text;
                c.Balance = decimal.Parse(tbBalance.Text);
                Globals.ctx.Customers.Add(c);
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchCustomersRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Customer customer = Globals.ctx.Customers.Where(c => c.id.ToString() == tbCustomerId.Text).FirstOrDefault();
                customer.FirstName = tbFN.Text;
                customer.LastName = tbLN.Text;
                customer.Address = tbAddress.Text;
                customer.PostalCode = tbPostCode.Text;
                customer.Telephone = tbPhone.Text;
                customer.Email = tbEmail.Text;
                customer.MemberCard = tbMemberCard.Text;
                customer.Balance = decimal.Parse(tbBalance.Text);
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchCustomersRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Customer customer = Globals.ctx.Customers.Where(c => c.id.ToString() == tbCustomerId.Text).FirstOrDefault();
                Globals.ctx.Customers.Remove(customer);
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchCustomersRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearInputs()
        {
            tbCustomerId.Text = "";
            tbFN.Text = "";
            tbLN.Text = "";
            tbAddress.Text = "";
            tbPostCode.Text = "";
            tbPhone.Text = "";
            tbEmail.Text = "";
            tbMemberCard.Text = "";
            tbBalance.Text = "";
            btnDialogAdd.IsEnabled = true;
            btnDialogUpdate.IsEnabled = false;
            btnDialogDelete.IsEnabled = false;
        }

        public bool IsFieldsValid()
        {
            if (tbCustomerId == null)
            {
                MessageBox.Show("Customer Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (tbFN.Text.Length < 2 && tbFN.Text.Length > 50)
            {
                MessageBox.Show("First Name must be between 2 and 50 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (tbLN.Text.Length < 2 && tbLN.Text.Length > 50)
            {
                MessageBox.Show("Last Name must be between 2 and 50 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!decimal.TryParse(tbBalance.Text, out decimal balance))
            {
                MessageBox.Show("Balance must be number", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
    }
}
