﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManageOrderDialog.xaml
    /// </summary>
    public partial class ManageOrderDialog : Window
    {
        private int m_currentOrderId;

        public int CurrentOrderId
        {
            get { return m_currentOrderId; }
            set { m_currentOrderId = value; }
        }

        public ManageOrderDialog(int orderId)
        {
            InitializeComponent();

            m_currentOrderId = orderId;
            try
            {
                FetchOrderItemRecords(m_currentOrderId);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.Close(); // fatal error
            }

        }

        public void FetchOrderItemRecords(int orderId)
        {
            try
            {

                lvOrderItemList.ItemsSource = Globals.ctx.OrderItems.Where(o => o.OrderID == orderId).ToList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void lvOrderItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnManagerOrderItem.IsEnabled = true;
            //btnDeleteOrderItem.IsEnabled = true;
        }

        private void lvOrderItemList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OrderItem orderItemSelected = (OrderItem)lvOrderItemList.SelectedItem;
            ManageOrderItemDialog mDlg = new ManageOrderItemDialog() { Owner = this };
            mDlg.tbOrderItemId.Text = orderItemSelected.OrderID.ToString();
            mDlg.tbProductId.Text = orderItemSelected.ProductID.ToString();
            mDlg.tbItemCount.Text = orderItemSelected.ItemCount.ToString();
            mDlg.tbPrice.Text = orderItemSelected.Price.ToString();
            mDlg.lblSubtotal.Content = (orderItemSelected.ItemCount * orderItemSelected.Price).ToString();
            mDlg.ShowDialog();

            // Update data.
            FetchOrderItemRecords(CurrentOrderId);

        }
        private void btnManagerOrderItem_Click(object sender, RoutedEventArgs e)
        {
            OrderItem orderItemSelected = (OrderItem)lvOrderItemList.SelectedItem;
            ManageOrderItemDialog mDlg = new ManageOrderItemDialog() { Owner = this };
            mDlg.tbOrderItemId.Text = orderItemSelected.OrderID.ToString();
            mDlg.tbProductId.Text = orderItemSelected.ProductID.ToString();
            mDlg.tbItemCount.Text = orderItemSelected.ItemCount.ToString();
            mDlg.tbPrice.Text = orderItemSelected.Price.ToString();
            mDlg.lblSubtotal.Content = (orderItemSelected.ItemCount * orderItemSelected.Price).ToString();
            mDlg.ShowDialog();
            // Update data.
            FetchOrderItemRecords(CurrentOrderId);
        }
        //private void btnDeleteOrderItem_Click(object sender, RoutedEventArgs e)
        //{

        //}
    }
}
