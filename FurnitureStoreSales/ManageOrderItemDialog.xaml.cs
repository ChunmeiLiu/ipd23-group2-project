﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManageOrderDialog.xaml
    /// </summary>
    public partial class ManageOrderItemDialog : Window
    {
        public ManageOrderItemDialog()
        {
            InitializeComponent();
        }
        
        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                OrderItem orderItem = Globals.ctx.OrderItems.Where(o => o.OrderID.ToString() == tbOrderItemId.Text).FirstOrDefault();
                orderItem.ProductID = int.Parse(tbProductId.Text);
                orderItem.ItemCount = int.Parse(tbItemCount.Text);
                orderItem.Price = decimal.Parse(tbPrice.Text);
                orderItem.SubTotal = orderItem.ItemCount * orderItem.Price;
                Globals.ctx.SaveChanges();
                this.Close();
                //((ManageOrderDialog)System.Windows.Application.Current.MainWindow).FetchOrderItemRecords(orderItem.OrderID);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                OrderItem orderItem = Globals.ctx.OrderItems.Where(o => o.OrderID.ToString() == tbOrderItemId.Text).FirstOrDefault();
                Globals.ctx.OrderItems.Remove(orderItem);
                Globals.ctx.SaveChanges();
                this.Close();
                //((ManageOrderDialog)System.Windows.Application.Current.MainWindow).FetchOrderItemRecords(orderItem.OrderID);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }        

        public bool IsFieldsValid()
        {
            if (tbOrderItemId == null)
            {
                MessageBox.Show("Order item Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!int.TryParse(tbOrderItemId.Text, out int orderItemId))
            {
                MessageBox.Show("Order item ID must be integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!int.TryParse(tbProductId.Text, out int productNumber))
            {
                MessageBox.Show("Product ID must be integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if(!int.TryParse(tbItemCount.Text, out int itemCount))
            {
                MessageBox.Show("Product item quantity must be integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!decimal.TryParse(tbPrice.Text, out decimal price))
            {
                MessageBox.Show("Product price must be decimal", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }


    }
}
