﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManageProductDialog.xaml
    /// </summary>
    public partial class ManageProductDialog : Window
    {
        byte[] currProductImage;
        public ManageProductDialog()
        {
            InitializeComponent();


        }
        public void ClearInputs()
        {
            imageViewer.Source = null;
            tbProductId.Text = "";
            tbProductName.Text = "";
            tbPrice.Text = "";
            tbCategory.Text = "";
            tbDescription.Text = "";
            tbPdtSupplierId.Text = "";
            btnDialogAdd.IsEnabled = true;
            btnDialogUpdate.IsEnabled = false;
            btnDialogDelete.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }

        public bool IsFieldsValid()
        {
            if (tbProductId == null)
            {
                MessageBox.Show("Product Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            Regex regexPName = new Regex("^[A-Za-z]{2,10}$");
            if (!regexPName.IsMatch(tbProductName.Text))
            {
                MessageBox.Show("Product name must be between 2 and 10 letters!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            Regex regexPCategory = new Regex("^[A-Za-z]{2,10}$");
            if (!regexPName.IsMatch(tbCategory.Text))
            {
                MessageBox.Show("Product category must be between 2 and 10 letters!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (currProductImage == null)
            {
                MessageBox.Show("Choose a picture", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btnDialogAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Product p = new Product();
                p.ProductPicture = currProductImage;
                p.ProductId = int.Parse(tbProductId.Text);
                p.ProductName = tbProductName.Text;
                p.Category = tbCategory.Text;
                p.Price = decimal.Parse(tbPrice.Text);
                p.ProductDescription = tbDescription.Text;
                p.SupplierId = int.Parse(tbPdtSupplierId.Text);

                Globals.ctx.Products.Add(p);
                Globals.ctx.SaveChanges();
                ClearInputs();
                //MainWindow.;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Product product = Globals.ctx.Products.Where(p => p.ProductId.ToString() == tbProductId.Text).FirstOrDefault();
                product.ProductPicture = currProductImage;
                product.ProductName = tbProductName.Text;
                product.Category = tbCategory.Text;
                product.Price = decimal.Parse(tbPrice.Text);
                product.ProductDescription = tbDescription.Text;
                product.SupplierId = int.Parse(tbPdtSupplierId.Text);
                Globals.ctx.SaveChanges();
                ClearInputs();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Product product = Globals.ctx.Products.Where(p => p.ProductId.ToString() == tbProductId.Text).FirstOrDefault();
                Globals.ctx.Products.Remove(product);
                Globals.ctx.SaveChanges();
                ClearInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currProductImage = File.ReadAllBytes(dlg.FileName);
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btnDialogSupplierDetail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Supplier supplier = Globals.ctx.Suppliers.Where(s => s.SupplierId.ToString() == tbPdtSupplierId.Text).FirstOrDefault();

                SupplierDetail sd = new SupplierDetail() { Owner = this };
                sd.tbSupplierId.Text = supplier.SupplierId.ToString();
                sd.tbSupplierName.Text = supplier.CompanyName;
                sd.tbSupplierAddress.Text = supplier.Address;
                sd.tbSupplierPostalCode.Text = supplier.PostalCode;
                sd.tbSupplierTelephone.Text = supplier.Telephone;
                sd.tbSupplierEmail.Text = supplier.Email;
                sd.tbSupplierMemo.Text = supplier.SupplierMemo;

                sd.lvSupProductList.ItemsSource =
                    Globals.ctx.Products.Where(p => p.SupplierId.ToString() == tbPdtSupplierId.Text).ToList();
                sd.tbSupplierId.IsReadOnly = true;
                sd.tbSupplierName.IsReadOnly = true;
                sd.tbSupplierAddress.IsReadOnly = true;
                sd.tbSupplierPostalCode.IsReadOnly = true;
                sd.tbSupplierTelephone.IsReadOnly = true;
                sd.tbSupplierEmail.IsReadOnly = true;
                sd.tbSupplierMemo.IsReadOnly = true;

                sd.ShowDialog();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
