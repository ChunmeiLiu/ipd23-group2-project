﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManagePurchaseDialog.xaml
    /// </summary>
    public partial class ManagePurchaseDialog : Window
    {
        public ManagePurchaseDialog()
        {
            InitializeComponent();
        }
        private void btnDialogAdd_Click(object sender, RoutedEventArgs e) //fix me
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Purchase p = new Purchase();
                p.id = int.Parse(tbPurchaseId.Text);
                p.ProductID = int.Parse(tbProductID.Text);
                p.ProductNumber = int.Parse(tbProductNumbers.Text);
                p.Total = (decimal)lblTotal.Content;
                p.OrderDate = DateTime.Parse(tbOrderDate.Text);
                if (DateTime.TryParse(tbPurchaseDate.Text, out DateTime pd))
                { p.PurchaseDate = DateTime.Parse(tbPurchaseDate.Text); }

                Globals.ctx.Purchases.Add(p);
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchPurchasesResultRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Purchase purchase = Globals.ctx.Purchases.Where(p => p.id.ToString() == tbPurchaseId.Text).FirstOrDefault();
                purchase.ProductID = int.Parse(tbProductID.Text);
                purchase.ProductNumber = int.Parse(tbProductNumbers.Text);
                //purchase.Total = Globals.ctx.Products.Where(p => p.ProductId == int.Parse(tbProductID.Text));
                purchase.OrderDate = DateTime.Parse(tbOrderDate.Text);
                if(DateTime.TryParse(tbPurchaseDate.Text,out DateTime pd))
                    { purchase.PurchaseDate = DateTime.Parse(tbPurchaseDate.Text);}                
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchPurchasesResultRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Purchase purchase = Globals.ctx.Purchases.Where(p => p.id.ToString() == tbPurchaseId.Text).FirstOrDefault();
                Globals.ctx.Purchases.Remove(purchase);
                Globals.ctx.SaveChanges();
                this.Close();
                //((MainWindow)System.Windows.Application.Current.MainWindow).FetchPurchasesResultRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearInputs()
        {
            tbPurchaseId.Text = "";
            tbProductID.Text = "";
            tbProductNumbers.Text = "";
            lblTotal.Content = "";
            tbOrderDate.Text = "MM/DD/YYYY";
            tbPurchaseDate.Text = "MM/DD/YYYY";
            
            btnDialogAdd.IsEnabled = true;
            btnDialogUpdate.IsEnabled = false;
            btnDialogDelete.IsEnabled = false;
        }

        public bool IsFieldsValid()
        {
            if (tbPurchaseId == null)
            {
                MessageBox.Show("Purchase Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if(!int.TryParse(tbProductID.Text, out int productID))
            {
                MessageBox.Show("Product ID must be integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!int.TryParse(tbProductNumbers.Text, out int productNumber))
            {
                MessageBox.Show("Product Quantity must be integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
    }
}
