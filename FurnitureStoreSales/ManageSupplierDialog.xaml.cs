﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for ManageSupplierDialog.xaml
    /// </summary>
    public partial class ManageSupplierDialog : Window
    {
        public ManageSupplierDialog()
        {
            InitializeComponent();
        }
        public void ClearInputs()
        {
            tbmpSupplierId.Text = "";
            tbmpSupplierName.Text = "";
            tbmpSupplierPostalCode.Text = "";
            tbSupplierTelephone.Text = "";
            tbmpSupplierMemo.Text = "";
            tbmpSupplierAddress.Text = "";
            tbmpSupplierEmail.Text = "";
            btnmpAdd.IsEnabled = true;
            btnmpUpdate.IsEnabled = false;
            btnmpDelete.IsEnabled = false;
        }

        public bool IsFieldsValid()
        {
            if (tbmpSupplierId == null)
            {
                MessageBox.Show("Supplier Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            Regex regexSName = new Regex("^[A-Za-z0-9]{2,50}$");
            if (!regexSName.IsMatch(tbmpSupplierName.Text))
            {
                MessageBox.Show("Company name must be between 2 and 50 characters in letters and digits!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


            Regex regexSPostalCode = new Regex("^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ][ ][0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$");
            if (!regexSPostalCode.IsMatch(tbmpSupplierPostalCode.Text))
            {
                MessageBox.Show("The format you entered is not correct!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            Regex regexSTelephone = new Regex("^[0-9]{3}[-][0-9]{4}$");
            if (!regexSPostalCode.IsMatch(tbSupplierTelephone.Text))
            {
                MessageBox.Show("The format you entered is not correct!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            Regex regexSEmail = new Regex("^[A-za-z0-9._]{3,30}[@][A-Za-z0-9]{2,10}[.][A-Za-z0-9]{2,10}$");
            if (!regexSEmail.IsMatch(tbmpSupplierEmail.Text))
            {
                MessageBox.Show("The format you entered is not correct!", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return true;
        }

        private void btnmpAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Supplier s = new Supplier();
                s.SupplierId = int.Parse(tbmpSupplierId.Text);
                s.CompanyName = tbmpSupplierName.Text;
                s.Address = tbmpSupplierAddress.Text;
                s.PostalCode = tbmpSupplierPostalCode.Text;
                s.Email = tbmpSupplierEmail.Text;
                s.SupplierMemo = tbmpSupplierMemo.Text;
                s.Telephone = tbSupplierTelephone.Text;

                btnmpAdd.IsEnabled = true;
                btnmpUpdate.IsEnabled = false;
                btnmpDelete.IsEnabled = false;

                Globals.ctx.Suppliers.Add(s);
                Globals.ctx.SaveChanges();
                ClearInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnmpUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Supplier supplier = Globals.ctx.Suppliers.Where(s => s.SupplierId.ToString() == tbmpSupplierId.Text).FirstOrDefault();
                supplier.CompanyName = tbmpSupplierName.Text;
                supplier.Address = tbmpSupplierAddress.Text;
                supplier.PostalCode = tbmpSupplierPostalCode.Text;
                supplier.Email = tbmpSupplierEmail.Text;
                supplier.SupplierMemo = tbmpSupplierMemo.Text;
                supplier.Telephone = tbSupplierTelephone.Text;
                Globals.ctx.SaveChanges();
                ClearInputs();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnmpDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx = new FurnitureStoreSalesEntities();
                Supplier supplier = Globals.ctx.Suppliers.Where(s => s.SupplierId.ToString() == tbmpSupplierId.Text).FirstOrDefault();
                Globals.ctx.Suppliers.Remove(supplier);
                Globals.ctx.SaveChanges();
                ClearInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnmpExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
