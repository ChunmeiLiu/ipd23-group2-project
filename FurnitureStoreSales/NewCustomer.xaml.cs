﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for NewCustomer.xaml
    /// </summary>
    public partial class NewCustomer : Window
    {
        public NewCustomer()
        {
            InitializeComponent();
        }
        /*
        private void FirstNameTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(FirstNameTextBox.Text))
            {
                Page1.CanSelectNextPage = false;
            }
            else
            {
                Page1.CanSelectNextPage = true;
            }
        }

        */
        private void tbBalance_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)//btnDialogAdd_Click(object sender, System.Windows.Controls.TextChangedEventArgs e)//RoutedEventArgs e)
        {

            if (string.IsNullOrEmpty(tbCustomerId.Text) || string.IsNullOrEmpty(tbFN.Text) || string.IsNullOrEmpty(tbLN.Text) || string.IsNullOrEmpty(tbAddress.Text) || string.IsNullOrEmpty(tbPostCode.Text) || string.IsNullOrEmpty(tbPhone.Text) || string.IsNullOrEmpty(tbEmail.Text) || string.IsNullOrEmpty(tbMemberCard.Text) || string.IsNullOrEmpty(tbBalance.Text)) { MessageBox.Show("All fileds are must not be empty"); }
            else
            {
                //foreach (Control c in this.Controls)
                //{
                //    if (c is TextBox)
                //    {
                //        TextBox textBox = c as TextBox;
                //        if (textBox.Text == string.Empty)
                //        {
                //            // Text box is empty.
                //            // You COULD store information about this textbox is it's tag.
                //        }
                //    }
                //}
                
                if (!IsFieldsValid()) { ClearInputs(); return; }
                try
                {

                    Customer c = new Customer();
                    c.id = int.Parse(tbCustomerId.Text);
                    c.FirstName = tbFN.Text;
                    c.LastName = tbLN.Text;
                    c.Address = tbAddress.Text;
                    c.PostalCode = tbPostCode.Text;
                    c.Telephone = tbPhone.Text;
                    c.Email = tbEmail.Text;
                    c.MemberCard = tbMemberCard.Text;
                    c.Balance = decimal.Parse(tbBalance.Text);
                    Globals.ctx.Customers.Add(c);
                    Globals.ctx.SaveChanges();
                    //this.Close();
                    //((MainWindow)System.Windows.Application.Current.MainWindow).FetchCustomersRecords();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                
                Page1.CanSelectNextPage = true;
            }
        }

        public bool IsFieldsValid()
        {
            if (tbCustomerId == null)
            {
                MessageBox.Show("Customer Id cannot be empty", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (tbFN.Text.Length < 2 && tbFN.Text.Length > 50)
            {
                MessageBox.Show("First Name must be between 2 and 50 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (tbLN.Text.Length < 2 && tbLN.Text.Length > 50)
            {
                MessageBox.Show("Last Name must be between 2 and 50 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!decimal.TryParse(tbBalance.Text, out decimal balance))
            {
                MessageBox.Show("Balance must be number", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        public void ClearInputs()
        {
            tbCustomerId.Text = "";
            tbFN.Text = "";
            tbLN.Text = "";
            tbAddress.Text = "";
            tbPostCode.Text = "";
            tbPhone.Text = "";
            tbEmail.Text = "";
            tbMemberCard.Text = "";
            tbBalance.Text = "";
            Page1.CanSelectNextPage = false;
        }


    }
}

