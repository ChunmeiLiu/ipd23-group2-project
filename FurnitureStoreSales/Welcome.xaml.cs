﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FurnitureStoreSales
{
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>
    public partial class Welcome : Window
    {

        public Welcome()
        {
            InitializeComponent();
        }

        private void btnWelcomeNext_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();

            if (rbAdmin.IsChecked == true)
            {
                Login login = new Login();
                login.Show();
                this.Close();
                main.btProductDetail.Visibility = Visibility.Hidden;
                main.btProductDetail.Visibility = Visibility.Collapsed;
                main.btSupplierDetail.Visibility = Visibility.Hidden;
                main.btSupplierDetail.Visibility = Visibility.Collapsed;


            }

            else if (rbOther.IsChecked == true)
            {
                main.Show();
                this.Close();
                main.btAddProduct.Visibility = Visibility.Hidden;
                main.btAddProduct.Visibility = Visibility.Collapsed;
                main.lvProductList.MouseDoubleClick -= 
                    new MouseButtonEventHandler(main.lvProductList_MouseDoubleClick);
                main.btAddSupplier.Visibility = Visibility.Hidden;
                main.btAddSupplier.Visibility = Visibility.Collapsed;
                main.lvSupplierList.MouseDoubleClick -= new MouseButtonEventHandler(main.lvSupplierList_MouseDoubleClick);
                main.btManageProduct.Visibility = Visibility.Hidden;
                main.btManageProduct.Visibility = Visibility.Collapsed;
                main.btManageSupplier.Visibility = Visibility.Hidden;
                main.btManageSupplier.Visibility = Visibility.Collapsed;
                main.btnManagerOrder.Visibility = Visibility.Hidden;
                main.btnManagerOrder.Visibility = Visibility.Collapsed;
                main.btnManagerOrder.Visibility = Visibility.Hidden;
                main.btnManagerOrder.Visibility = Visibility.Collapsed; 
                main.btnManageCustomer.Visibility = Visibility.Hidden;
                main.btnManageCustomer.Visibility = Visibility.Collapsed;






            }
            else
            {
                const string message = "You don't pick an option!";
                const string caption = "Message";
                MessageBox.Show(message, caption);
            }

        }

        private void btnWelcomeExit_Click(object sender, RoutedEventArgs e)
        {
            const string message = "Are you sure you would like to leave?";
            const string caption = "You are leaving";
            MessageBox.Show(message, caption, MessageBoxButton.YesNo);
       }
    }
}
