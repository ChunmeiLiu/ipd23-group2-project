﻿#pragma checksum "..\..\ManagerSupplierDialog.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "015C8C0B08D2AFEA60F6C5C0C15DF1347ED98100AEF3F29F0734654EFB1AC31C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FurnitureStoreSales;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FurnitureStoreSales {
    
    
    /// <summary>
    /// ManagerSupplierDialog
    /// </summary>
    public partial class ManagerSupplierDialog : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierId;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierName;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierAddress;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierPostalCode;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSupplierTelephone;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnmpExit;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierEmail;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbmpSupplierMemo;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnmpAdd;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnmpUpdate;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\ManagerSupplierDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnmpDelete;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FurnitureStoreSales;component/managersupplierdialog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ManagerSupplierDialog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbmpSupplierId = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.tbmpSupplierName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.tbmpSupplierAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.tbmpSupplierPostalCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.tbSupplierTelephone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.btnmpExit = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\ManagerSupplierDialog.xaml"
            this.btnmpExit.Click += new System.Windows.RoutedEventHandler(this.btnmpExit_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.tbmpSupplierEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.tbmpSupplierMemo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.btnmpAdd = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\ManagerSupplierDialog.xaml"
            this.btnmpAdd.Click += new System.Windows.RoutedEventHandler(this.btnmpAdd_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnmpUpdate = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\ManagerSupplierDialog.xaml"
            this.btnmpUpdate.Click += new System.Windows.RoutedEventHandler(this.btnmpUpdate_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnmpDelete = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\ManagerSupplierDialog.xaml"
            this.btnmpDelete.Click += new System.Windows.RoutedEventHandler(this.btnmpDelete_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

